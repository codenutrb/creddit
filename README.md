# Creddit - A simple interface to post and vote topics

## Overview

This project is a simple clone of reddit where user can add, up vote or down vote posts.

The project is using the [Java Dropwizard Framework](http://www.dropwizard.io/).

## Requirements

This project uses the Java 8 and please install [Java Development Kit 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
before running the application

## Running the application
 * Clone the app repository on your local machine

        git clone https://bitbucket.org/codenutrb/creddit/

 * Go to the application folder

        cd creddit

 * To test and package the application run.

        ./gradlew clean build

 * To run the server run.

        java -jar build/libs/creddit-1.0.0-all.jar server dropwizard.yml


* Then open this url on your browser.

        http://localhost:8080/

## Demo

See it live in action [here](https://credditme.herokuapp.com/)