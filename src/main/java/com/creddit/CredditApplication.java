package com.creddit;

import com.creddit.dao.impl.PostDaoImpl;
import com.creddit.resources.api.PostApiResource;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class CredditApplication extends Application<CredditConfiguration> {
  @Override
  public void run(CredditConfiguration configuration, Environment environment) {
    final PostApiResource resource = new PostApiResource(new PostDaoImpl());
    environment.getObjectMapper()
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        .registerModule(new JodaModule());
    environment.jersey().register(resource);
  }

  @Override
  public void initialize(Bootstrap<CredditConfiguration> bootstrap) {
    bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
  }

  public static void main(String[] args) throws Exception {
    new CredditApplication().run(args);
  }
}
