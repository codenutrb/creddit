package com.creddit.resources.api;

import com.creddit.dao.PostDao;
import com.creddit.models.Post;
import java.util.List;
import javax.annotation.Nonnegative;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/posts")
@Produces(MediaType.APPLICATION_JSON)
public class PostApiResource {

  private PostDao postDao;

  public PostApiResource(PostDao postDao) {
    this.postDao = postDao;
  }

  @POST
  public Post save(@Valid Post post) {
    return postDao.save(post);
  }

  @GET
  @Path("/{id}/vote/{voteValue}")
  public Post upVote(@PathParam("id") @Min(value = 1) long id, @PathParam("voteValue") int voteValue) {
    Post post = postDao.getById(id).orElseThrow(() -> new NotFoundException("Post not found."));
    return postDao.vote(post, voteValue);
  }

  @GET
  @Path("/popular")
  public List<Post> popular(@QueryParam("limit") @DefaultValue("20") int limit) {
    return postDao.getPopular(limit);
  }
}
