package com.creddit.dao;

import com.creddit.models.Post;
import java.util.List;
import java.util.Optional;

public interface PostDao {
  Post save(Post post);

  List<Post> getPopular(int limit);

  Post vote(Post post, int vote);

  Optional<Post> getById(long id);

  int count();
}
