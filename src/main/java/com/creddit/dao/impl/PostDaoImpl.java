package com.creddit.dao.impl;

import com.creddit.dao.PostDao;
import com.creddit.models.Post;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PostDaoImpl implements PostDao {
  // in memory store for mapping the posts to its number of votes as the key
  // use sorted map for faster access with time complexity of O(log n)
  // for lookup, retrieve and delete operations
  private SortedMap<Long, SortedSet<Post>> votesToPosts;

  // use this to maintain a mapping of the post to its id as the key
  // this will be used for getById method
  private Map<Long, Post> idToPost;

  // for auto incremented id. PostDaoImpl will only have instance for this app
  // and should use atomic long for concurrent access in multi-threaded environment
  private final AtomicLong idCounter;

  public PostDaoImpl() {
    idToPost = new ConcurrentHashMap<>();
    // keys are sorted in descending order
    votesToPosts = Collections
        .synchronizedSortedMap(new TreeMap<>((v1, v2) -> Long.compare(v2, v1)));
    idCounter = new AtomicLong();
  }

  /**
   * Get the Post that matches the id
   *
   * @param id the id of the post to get
   * @return Optional<Post> an Optional wrapper of Post object that matches the id
   */
  public Optional<Post> getById(long id) {
    return Optional.ofNullable(idToPost.get(id));
  }

  /**
   * Save the post into in memory storage
   *
   * @param post the Post object that will be saved
   * @return Post> an instance of Post object that was saved
   */
  public Post save(Post post) {
    post.setId(idCounter.incrementAndGet());

    // save the post object on the map with the post id as the key
    idToPost.put(post.getId(), post);

    // save the post on the map that will maintain the order of
    // posts based on the number of votes
    put(votesToPosts, post);
    return post;
  }

  /**
   * Update the number of votes of the post
   *
   * @param post the Post object that will update the number of votes
   * @param vote the value to be added on votes of a Post. should only be 1 or -1
   * @return Post> an instance of Post object that was updated
   */
  public Post vote(Post post, int vote) {
    if (vote != 1 && vote != -1) {
      throw new IllegalArgumentException("Only 1 and -1 are valid values for vote");
    }

    // first remove the post from the map bucket using its current vote as the key
    remove(votesToPosts, post);

    post.setVotes(post.getVotes() + vote);

    // update the post object the id to post map
    idToPost.put(post.getId(), post);

    // put the post on the map bucket using the newly update votes as the key
    put(votesToPosts, post);

    return post;
  }

  /**
   * Get the most popular posts
   *
   * @param limit the limit of the number of posts. should be greater than 0
   * @return List<Post> the list of most popular posts
   */
  public List<Post> getPopular(int limit) {
    if (limit <= 0) {
      throw new IllegalArgumentException("Limit should be greater than 0");
    }

    List<Post> posts = new ArrayList<>();

    // loop through the map with votes as the key and set of posts as the value
    // the keys are sorted in descending order
    // for each set of posts, iterate to each post and add to the list of popular post
    // decrement limit by and keep doing this until limit is 0
    for (long key : votesToPosts.keySet()) {
      Iterator<Post> iterator = votesToPosts.get(key).iterator();
      while (limit > 0 && iterator.hasNext()) {
        posts.add(iterator.next());
        limit -= 1;
      }
      if (limit <= 0) {
        break;
      }
    }

    return posts;
  }

  /**
   * Get the total number of posts
   *
   * @return int the total number of posts
   */
  public int count() {
    return idToPost.size();
  }

  private void put(Map<Long, SortedSet<Post>> map, Post post) {
    // check first if votes is currently on the keys of the map
    if (!map.containsKey(post.getVotes())) {
      // put an entry on the map where votes as the key
      // and TreeSet as the value
      // need to synchronize the set for concurrency
      map.put(post.getVotes(), Collections.synchronizedSortedSet(new TreeSet<>(
          Comparator.comparing(Post::getId))));
    }
    map.get(post.getVotes()).add(post);
  }

  private void remove(Map<Long, SortedSet<Post>> map, Post post) {
    // remove the post for the map entry
    if (map.containsKey(post.getVotes())) {
      map.get(post.getVotes()).remove(post);
    }
  }
}
