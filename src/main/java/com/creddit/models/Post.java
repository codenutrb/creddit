package com.creddit.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;

public class Post {
  @JsonProperty
  private long id;

  @JsonProperty
  @Length(max = 255, min = 1)
  private String topic;

  @JsonProperty
  private long votes;

  private DateTime createdAt;

  public Post() {
    this.createdAt = DateTime.now();
  }

  public Post(String topic) {
    this();
    this.topic = topic;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public long getVotes() {
    return votes;
  }

  public void setVotes(long votes) {
    this.votes = votes;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getId());
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Post) {
      return this.id == ((Post) o).getId();
    }
    return false;
  }

  public DateTime getCreatedAt() {
    return createdAt;
  }
}
