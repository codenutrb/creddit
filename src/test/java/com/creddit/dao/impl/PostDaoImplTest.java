package com.creddit.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.creddit.dao.PostDao;
import com.creddit.models.Post;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PostDaoImplTest {

  @Test
  public void testGetById() {
    PostDao postDaoImpl = new PostDaoImpl();
    Post post = new Post("testGet");
    postDaoImpl.save(post);

    Optional<Post> getPost = postDaoImpl.getById(post.getId());
    assertNotEquals(Optional.empty(), getPost);
    assertEquals(post, getPost.get());
  }

  @Test
  public void testGetById_NotFound() {
    PostDao postDaoImpl = new PostDaoImpl();
    Optional<Post> post = postDaoImpl.getById(1);

    assertEquals(Optional.empty(), post);
  }

  @Test
  public void testSave() {
    PostDao postDaoImpl = new PostDaoImpl();
    postDaoImpl.save(new Post("test"));

    assertEquals(1, postDaoImpl.count());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testVote_IllegalValueForVote() {
    PostDao postDaoImpl = new PostDaoImpl();
    Post post = new Post("abc");
    postDaoImpl.vote(post, 2);
  }

  public void testVote() {
    PostDao postDao = new PostDaoImpl();
    Post post = new Post("abc");

    post = postDao.vote(post, 1);

    assertEquals(1, post.getVotes());
  }

  @Test
  public void testPopular() {
    PostDao postDaoImpl = new PostDaoImpl();
    Post post1 = new Post("test1");
    Post post2 = new Post("test2");
    Post post3 = new Post("test3");

    postDaoImpl.save(post1);
    postDaoImpl.save(post2);
    postDaoImpl.save(post3);

    postDaoImpl.vote(post1, 1);

    List<Post> popular = postDaoImpl.getPopular(2);
    assertEquals(2, popular.size());
    assertEquals(1, popular.get(0).getVotes());
    assertEquals(0, popular.get(1).getVotes());
  }

  @Test
  public void testPopular_LimitGreaterThanSize() {
    PostDao postDaoImpl = new PostDaoImpl();
    Post post1 = new Post("test");
    Post post2 = new Post("test2");
    Post post3 = new Post("test3");

    postDaoImpl.save(post1);
    postDaoImpl.save(post2);
    postDaoImpl.save(post3);

    List<Post> popular = postDaoImpl.getPopular(4);
    assertEquals(3, popular.size());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testPopular_NonPositiveLimit() {
    PostDao postDaoImpl = new PostDaoImpl();
    postDaoImpl.getPopular(0);
  }
}
