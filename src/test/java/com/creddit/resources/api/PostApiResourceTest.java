package com.creddit.resources.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.creddit.dao.PostDao;
import com.creddit.dao.impl.PostDaoImpl;
import com.creddit.models.Post;
import com.google.common.collect.Lists;
import io.dropwizard.testing.junit.ResourceTestRule;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PostApiResourceTest {

  private static final PostDao postDao = mock(PostDaoImpl.class);

  @ClassRule
  public static final ResourceTestRule resource = ResourceTestRule.builder()
      .addResource(new PostApiResource(postDao)).build();

  @Test
  public void save() {
    Post post = new Post("test post");
    when(postDao.save(any(Post.class))).thenReturn(post);

    Response response = resource.client().target("/posts").request(MediaType.APPLICATION_JSON)
        .post(Entity.entity(Post.class, MediaType.APPLICATION_JSON_TYPE));

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
  }

  @Test
  public void testUpVote_NotFound() {
    when(postDao.getById(anyLong())).thenReturn(Optional.empty());

    Response response = resource.client().target("/posts/id/vote/-1").request().get();
    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_FOUND);
  }

  @Test
  public void testUpvote() {
    Post post = new Post("test");
    post.setId(1);
    when(postDao.getById(anyLong())).thenReturn(Optional.of(post));
    when(postDao.vote(post, -1)).thenReturn(post);

    Post newPost = resource.client().target("/posts/1/vote/-1").request().get(Post.class);

    assertThat(post).isNotNull();
    assertThat(post).isEqualTo(newPost);
  }

  @Test
  public void testPopular() {
    when(postDao.getPopular(anyInt()))
        .thenReturn(Lists.newArrayList(new Post("a"), new Post("b"), new Post("c")));

    List<Post> popular = resource.client().target("/posts/popular").queryParam("limit", 2).request()
        .get(List.class);

    assertThat(popular.size()).isEqualTo(3);
  }
}